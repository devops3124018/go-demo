package api

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func Hostname(c *gin.Context) {
	hostname, err := os.Hostname()
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"code": -1, "errMsg": err})
		return
	}

	c.JSON(http.StatusOK, gin.H{"code": 0, "msg": hostname})
}
