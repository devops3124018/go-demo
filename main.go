package main

import (
	"go-demo/router"
)

func main() {
	r := router.SetupRouter()
	// Listen
	r.Run(":8080")
}
