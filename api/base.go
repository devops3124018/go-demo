package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Ping(c *gin.Context) {
	c.String(http.StatusOK, "pong\n")
}

func GetVersion(c *gin.Context) {
	c.String(http.StatusOK, "v1.0\n")
}
