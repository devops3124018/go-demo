package router

import (
	"go-demo/api"

	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()

	// ping
	r.GET("/ping", api.Ping)

	// version
	r.GET("/version", api.GetVersion)

	apiV1Router := r.Group("/api/v1")

	// 获取hostname
	apiV1Router.GET("/hostname", api.Hostname)

	return r
}
